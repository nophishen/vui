/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 顶级父类,提供一些公共方法
 * @class VUI.Common
 */
VUI.Class('Common',{
	// 默认属性
	OPTS:{
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this.opts = $.extend({},this.OPTS,opts);
	}
	/**
	 * 返回默认属性
	 * @return {Object} 默认属性
	 */
	,getDefOpts:function(){
		return this.OPTS;
	}
	/**
	 * 设置/获取属性
	 * @param {String} optName 属性名
	 * @param {Object} val 属性值
	 */
	,opt:function(optName,val) {
		if(optName && typeof optName === 'string') {
			if(val !== undefined) {
				this.opts[optName] = val;
			}else{
				return this.opts[optName];
			}
		}
	}
	/**
	 * 设置事件
	 * @param {String} eventName 事件名,首字母大写
	 * @param {Function} fn 事件方法
	 */
	,on:function(eventName,fn) {
		this.opt('on' + eventName,fn);
	}
	/**
	 * 设置/获取属性,同opt(optName,val)
	 * @param {String} name 属性名
	 * @param {Object} val 属性值
	 */
	,set:function(name,val) {
		this.opt(name,val);
	}
	/**
	 * 返回属性对象
	 * @return {Object}
	 */
	,getOpts:function() {
		return this.opts;
	}
	/**
	 * 作用同getOpts()
	 */
	,options:function() {
		return this.getOpts();
	}
	/**
	 * 触发一个事件
	 * @param {String} eventName 事件名
	 */
	,fire:function(eventName) {
		var eventFun = this.opts['on' + eventName];
		if(eventFun) {
			var argus = [];
			for(var i=1, len=arguments.length;i<len; i++) {
				argus.push(arguments[i]);
			}
			return eventFun.apply(this,argus);
		}
	}
	/**
	 * 运行一个配置参数上的方法
	 * var win = new VUI.Win({handler:function(){}});<br>
	 * win.runOptFun('handler')
	 * @param {String} funName 方法名
	 */
	,runOptFun:function(funName) {
		var fun = this.opts[funName];
		if(fun) {
			var argus = [];
			for(var i=1, len=arguments.length;i<len; i++) {
				argus.push(arguments[i]);
			}
			return fun.apply(this,argus);
		}
	}
	
});

})();