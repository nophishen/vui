/*
 * Copyright (c) 2016 VUI(https://git.oschina.net/durcframework/vui) All rights reserved.
 */

;(function(){

/**
 * 页面表单控件,包含了增删改查功能<br>
 <pre>
pageForm = new VUI.PageForm({
	formId:'form'
	,grid:grid
	,win:win
	,crudUrl:{
		add:addUrl
	}
});
 </pre>
 * @class VUI.PageForm
 * @extends VUI.Form
 */
VUI.Class('PageForm',{
	// 默认属性
	OPTS:{
		grid:null
		,win:null
		,crudUrl:null
	}
	/**
	 * 构造函数
	 * @constructor
	 * @ignore
	 */
	,init:function(opts) {
		this._super(opts);
	}
	/**
	 * 添加
	 */
	,add:function() {
		this.setUrl(this.getCrudUrl().add);
		this.clear();
		this.getWin().setTitle('添加');
		this.getWin().show();
	}
	/**
	 * 修改
	 * @param {Object} data 表格行数据
	 */
	,update:function(data) {
		this.getWin().setTitle('修改');
		this.setUrl(this.getCrudUrl().update);
		this.clear();
		this.loadData(data);
		this.getWin().show();
	}
	/**
	 * 删除
	 * @param {Object} data 表格行数据
	 */
	,del:function(data) {
		var self = this;
		VUI.Msg.confirm('确认','确定删除这条记录?',function(r){
			if(r){
		    	var url = self.getCrudUrl().del;
				self.submit({
					url:url
					,data:data
					,onSubmit:function(){}
					,success: function(data){
						if(data.success) {							
							self.getGrid().reload();							
						}else{
							VUI.Msg.alert('提示',data.message);
						}
					}
				});
			}
		});
	}
	/**
	 * 保存
	 * @param {String} opts 保存选项<br>
	 * {onSubmit:function(data){},success:function:(data){}}
	 */
	,save:function(opts) {
		opts = opts || {}
		var self = this;
		var url = this.getUrl();
		this.submit({
			url:url
			,onSubmit: function(data){
				if(opts.onSubmit) {
					return opts.onSubmit.call(self,data);
				}else{
					return this.validate();	// 返回false终止表单提交
				}
			}
			,success: function(data){
				if(data.success) {
					if(opts.success) {
						return opts.success.call(self,data);
					}else{
						self.getGrid().reload();
						self.getWin().close();
					}
				}else{
					VUI.Msg.alert('提示',data.message);
				}
			}
		});
		
	}
	/**
	 * 设置提交的url
	 * @param {String} url url
	 */
	,setUrl:function(url) {
		this.opt('url',url);
	}
	/**
	 * 返回提交的url
	 * @return 提交的url
	 */
	,getUrl:function() {
		return this.opts.url;
	}
	/**
	 * 获取增删改查的url
	 * @return 返回一个json对象,如:
	 * {
		add:ctx + '/addStudent.do'
		,update:ctx + '/updateStudent.do'
		,del:ctx + '/delStudent.do'
		}
	 */
	,getCrudUrl:function() {
		return this.opts.crudUrl;
	}
	/**
	 * 获取win控件
	 */
	,getWin:function() {
		return this.opts.win;
	}
	/**
	 * 获取grid控件
	 */
	,getGrid:function() {
		return this.opts.grid;
	}
},VUI.Form);

})();